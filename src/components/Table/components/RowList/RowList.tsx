import { FC } from 'react';
import { useQuery } from '@apollo/client';

import { missionQuery } from 'constants/queries';
import Loader from 'components/Loader';

const RowList: FC<{ id: string }> = ({ id }) => {
  const { data, loading } = useQuery(missionQuery, {
    variables: {
      id,
    },
  });
  if (loading) return <Loader />;
  return data ? <li className='mb-2'>{data.mission.description}</li> : null;
};

export default RowList;
