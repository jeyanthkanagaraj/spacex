module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      blue: '#4D77FF',
      lightGray: '#CCCCCC',
      white: '#FFFFFF',
      yellow: '#F5E216',
    },
    extend: {},
  },
  plugins: [],
};
