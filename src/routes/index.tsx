import { FC, lazy, Suspense } from 'react';
import { BrowserRouter, Routes as Switch, Route } from 'react-router-dom';

const Home = lazy(() => import('views/Home'));

const Routes: FC = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<p>Loading...</p>}>
        <Switch>
          <Route path='/' element={<Home />} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
};

export default Routes;
