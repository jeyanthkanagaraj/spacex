import { useState, useMemo } from 'react';
import { SortableOutputPropsType } from 'constants/types';

export const useSortableData = (
  items: any,
  config: { key: string; direction: string } | null = null
): SortableOutputPropsType => {
  const [SortConfig, setSortConfig] = useState(config);

  //Sorting content based on the key and direction
  const sortedItems = useMemo(() => {
    let sortableItems = [...items];
    if (SortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (SortConfig.key !== undefined) {
          if (a[SortConfig.key] < b[SortConfig.key]) {
            return SortConfig.direction === 'ascending' ? -1 : 1;
          }
          if (a[SortConfig.key] > b[SortConfig.key]) {
            return SortConfig.direction === 'ascending' ? 1 : -1;
          }
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, SortConfig]);

  //Setting sorting direction
  const requestSort = (key: string) => {
    let direction = 'ascending';
    if (
      SortConfig &&
      SortConfig.key === key &&
      SortConfig.direction === 'ascending'
    ) {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  };

  return { items: sortedItems, requestSort, SortConfig };
};
