export interface LaunchPropsType {
  id: number;
  launch_date_utc: string;
  mission_name: string;
  mission_id: string[];
}

export interface TableHeaderLabelType {
  id: number;
  label: string;
  key: { key: string } | string;
}

export interface TablePropsType {
  headerLabel: TableHeaderLabelType[];
  rowData: LaunchPropsType[];
  handleSelectFavourite: Function;
  favourites: number[];
  handleRemoveFavourite: Function;
}

export interface TableRowPropsType {
  launch: LaunchPropsType;
  favourites: number[];
  handleSelectFavourite: Function;
  handleRemoveFavourite: Function;
}

export interface SortableOutputPropsType {
  items: LaunchPropsType[];
  requestSort: Function;
  SortConfig: { key: string; direction: string } | null;
}
