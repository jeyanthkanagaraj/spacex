import { gql } from '@apollo/client';

//To get launches data
export const launchesQuery = gql`
  query LaunchesQuery {
    launches(limit: 10) {
      id
      launch_date_utc
      mission_name
      mission_id
    }
  }
`;

//To get a specific mission data
export const missionQuery = gql`
  query MissionQuery($id: ID!) {
    mission(id: $id) {
      description
      id
    }
  }
`;
