import Routes from './routes';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import 'react-toastify/dist/ReactToastify.css';

const client = new ApolloClient({
  uri: 'https://api.spacex.land/graphql/',
  cache: new InMemoryCache(),
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Routes />
    </ApolloProvider>
  );
}

export default App;
