import { FC } from 'react';
import { DateTime } from 'luxon';
import StarIcon from '@heroicons/react/solid/StarIcon';
import StarOutlineIcon from '@heroicons/react/outline/StarIcon';

import RowList from '../RowList';
import { TableRowPropsType } from 'constants/types';

const TableRow: FC<TableRowPropsType> = ({
  launch: { id, launch_date_utc, mission_name, mission_id },
  favourites,
  handleSelectFavourite,
  handleRemoveFavourite,
}) => {
  return (
    <tr>
      <td className='border border-lightGray px-8 py-4 w-1/6'>
        {favourites.includes(id) ? (
          <StarIcon
            className='w-12 h-12 text-yellow cursor-pointer mx-auto'
            onClick={() => handleRemoveFavourite(id)}
          />
        ) : (
          <StarOutlineIcon
            className='w-10 h-10 text-lightGray cursor-pointer mx-auto'
            onClick={() => handleSelectFavourite(id)}
          />
        )}
      </td>
      <td className='border border-lightGray px-8 py-4 w-1/6'>
        {DateTime.fromISO(launch_date_utc).toFormat('dd MMMM yyyy, HH:mm')}
      </td>
      <td className='border border-lightGray px-8 py-4 w-1/6'>
        {mission_name}
      </td>
      <td className='border border-lightGray px-8 py-4 w-3/6'>
        <ul className='list-disc'>
          {mission_id?.length
            ? mission_id.map((id: string) => <RowList key={id} id={id} />)
            : 'NA'}
        </ul>
      </td>
    </tr>
  );
};

export default TableRow;
