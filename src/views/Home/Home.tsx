import { FC, useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useQuery } from '@apollo/client';

import Header from 'components/Header';
import { launchesQuery } from 'constants/queries';
import Table from 'components/Table';
import Loader from 'components/Loader';
import { TableHeaderLabelType } from 'constants/types';

const Home: FC = () => {
  const [TableHeader] = useState<TableHeaderLabelType[]>([
    { id: 0, label: 'Favourite', key: 'favourite' },
    { id: 1, label: 'Date', key: 'launch_date_utc' },
    { id: 2, label: 'Mission Name', key: 'mission_name' },
    { id: 3, label: 'Description', key: 'description' },
  ]);
  const [Favourites, setFavourites] = useState<number[]>([]);
  const { data, loading } = useQuery(launchesQuery);

  const handleSelectFavourite = (id: number) => {
    let favourites = [...Favourites, id];
    toast.success('Flight added to your favourites successfully');
    localStorage.setItem('favourites', JSON.stringify(favourites));
    setFavourites(favourites);
  };

  const handleRemoveFavourite = (id: number) => {
    let favourites = Favourites.filter((favourite) => favourite !== id);
    toast.success('Flight removed from your favourites successfully');
    localStorage.setItem('favourites', JSON.stringify(favourites));
    setFavourites(favourites);
  };

  useEffect(() => {
    let favourites = localStorage.getItem('favourites');
    if (favourites) {
      setFavourites(JSON.parse(favourites));
    }
  }, []);

  return (
    <div className='w-full sm:w-3/4 mx-auto'>
      <Header />
      <div className='mt-10 mx-auto flex justify-center mb-6'>
        {loading ? (
          <Loader />
        ) : (
          <>
            {data?.launches?.length ? (
              <div className='w-[95%] sm:w-full mx-auto overflow-auto'>
                <Table
                  rowData={data?.launches}
                  headerLabel={TableHeader}
                  handleSelectFavourite={handleSelectFavourite}
                  favourites={Favourites}
                  handleRemoveFavourite={handleRemoveFavourite}
                />
              </div>
            ) : (
              <p className='text-2xl'>No items to show</p>
            )}
          </>
        )}
      </div>
      <ToastContainer autoClose={1500} className='text-color' />
    </div>
  );
};

export default Home;
