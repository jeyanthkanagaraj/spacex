import { FC } from 'react';

const Header: FC = () => {
  return (
    <div className='border-b border-lightGray py-4'>
      <h2 className='text-center text-4xl font-bold text-blue'>
        Space<span className='italic text-lightGray'>X</span>
      </h2>
    </div>
  );
};

export default Header;
