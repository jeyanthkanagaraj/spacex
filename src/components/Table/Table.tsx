import { FC } from 'react';
import TableRow from './components/TableRow';
import { useSortableData } from 'customHooks/useSortableDate';
import ChevronUp from '@heroicons/react/solid/ChevronUpIcon';
import ChevronDown from '@heroicons/react/solid/ChevronDownIcon';
import {
  TablePropsType,
  TableHeaderLabelType,
  LaunchPropsType,
  SortableOutputPropsType,
} from 'constants/types';
import './style.css';

const Table: FC<TablePropsType> = ({
  headerLabel,
  rowData,
  handleSelectFavourite,
  favourites,
  handleRemoveFavourite,
}) => {
  const { items, requestSort, SortConfig }: SortableOutputPropsType =
    useSortableData(rowData);

  return (
    <table className='shadow-lg bg-white border-separate'>
      <thead>
        <tr>
          {headerLabel &&
            headerLabel.map((label: TableHeaderLabelType) => (
              <th
                className='bg-blue border border-lightGray text-left px-8 py-4 text-white custom-header-cell'
                key={label.id}
                onClick={
                  label.id === 1 || label.id === 2
                    ? () => requestSort(label.key)
                    : undefined
                }
              >
                <div className='flex items-center  min-h-[28px]'>
                  <span>{label.label}</span>{' '}
                  {(label.id === 1 || label.id === 2) && (
                    <>
                      {SortConfig && SortConfig.key === label.key ? (
                        <>
                          {SortConfig.direction === 'ascending' ? (
                            <ChevronDown className='w-5 h-5 text-lightGray' />
                          ) : (
                            <ChevronUp className='w-5 h-5 text-lightGray' />
                          )}
                        </>
                      ) : (
                        <span className='hidden custom-sorting'>
                          <ChevronUp className='w-5 h-5 -mb-3 text-lightGray' />
                          <ChevronDown className='w-5 h-5 -mt-3 text-lightGray' />
                        </span>
                      )}
                    </>
                  )}
                </div>
              </th>
            ))}
        </tr>
      </thead>
      <tbody>
        {items &&
          items.map((launch: LaunchPropsType) => (
            <TableRow
              key={launch.id}
              launch={launch}
              handleSelectFavourite={handleSelectFavourite}
              favourites={favourites}
              handleRemoveFavourite={handleRemoveFavourite}
            />
          ))}
      </tbody>
    </table>
  );
};

export default Table;
