# A Simple React Table App using SpaceX graphQL API

# Prerequisite

- Nodejs v14+
- NPM v6+

## Quick Start

```
git clone https://gitlab.com/jeyanthkanagaraj/spacex.git

cd spacex

npm install or yarn install

npm start or yarn start
```

# Libraries used

- @apollo/client: "^3.5.10",
- @heroicons/react: "^1.0.6",
- graphql: "^16.3.0",
- luxon: "^2.3.1",
- react-router-dom: "^6.2.2",
- react-toastify: "^8.2.0",
- typescript: "^4.6.2",
- postcss: "^8.4.8",
- tailwindcss: "^3.0.23"
